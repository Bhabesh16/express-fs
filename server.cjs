const express = require('express');

const errorHandler = require('./middleware/errorHandler.cjs');
const createDirectoryRoute = require('./routes/createDirectory.cjs');
const deleteFilesRoute = require('./routes/deleteFiles.cjs');

const app = express();
const PORT = process.env.PORT || 3030;

app.use(express.json());

app.use('/create-directory', createDirectoryRoute);

app.use('/delete-files', deleteFilesRoute);

app.all('*', (req, res, next) => {
    next([404, 'Invalid URL']);
})

app.use(errorHandler);

app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`);
});