const express = require('express');

const errorHandler = require('../middleware/errorHandler.cjs');
const createNewDirectory = require('../utils/createNewDirectory.cjs');
const createNewFiles = require('../utils/createNewFiles.cjs');

const router = express.Router();

router.post('/', (req, res, next) => {

    if (req.headers['content-type'] !== "application/json") {
        next([400, "content not in json format"]);
    } else {

        const { directoryName, fileList } = req.body;

        if (typeof directoryName === 'string' && Array.isArray(fileList)) {

            createNewDirectory(directoryName)
                .then(() => {
                    return createNewFiles(directoryName, fileList);
                })
                .then((successMessage) => {
                    res.status(successMessage[0]).json(successMessage[1]);
                })
                .catch((err) => {
                    next(err);
                });
        } else {
            console.error(new Error('Invalid request body'));
            next([400, 'Invalid request body']);
        }
    }
});

router.use(errorHandler);

module.exports = router;