const errorHandler = (err, req, res, next) => {

    const errorMessage = {
        message: 'Internal server error'
    };
    let errorStatus = 500;

    if (!Array.isArray(err)) {
        errorMessage.message = err.message;
        errorStatus = 400;
    } else {
        if (err[1].includes('EACCES')) {
            errorMessage.message = 'permission denied, cannot have access'
            errorStatus = 403;
        } else if (err[1].includes('ENOENT')) {
            errorMessage.message = 'file not found';
            errorStatus = 404;
        } else {
            errorMessage.message = err[1];
            errorStatus = err[0];
        }
    }

    return res.status(errorStatus).json(errorMessage);
}

module.exports = errorHandler;