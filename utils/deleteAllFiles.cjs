const fs = require('fs');
const path = require('path');

function deleteAllFiles(fileList, directoryName) {

    return new Promise((resolve, reject) => {

        const promiseDeleteFiles = fileList.map((fileName) => {

            return new Promise((resolve, reject) => {

                if (typeof fileName !== 'string' || fileName.trim().length < 1 || fileName.includes('.') || fileName.includes('/')) {
                    console.error(new Error("File name is incorrect"));
                    reject([400, 'file name is incorrect in fileList']);
                } else {

                    const filePath = path.join(__dirname, '../public', directoryName, `${fileName}.json`);

                    fs.unlink(filePath, (err) => {
                        if (err) {
                            console.log(err);
                            reject([404, err.message]);
                        } else {
                            resolve(fileName);
                        }
                    });
                }
            });
        });

        Promise.allSettled(promiseDeleteFiles)
            .then((promiseFiles) => {
                const successMessage = {
                    message: 'some files deleted'
                };

                let status = 206;
                let isError = [];

                const fullfilledFiles = promiseFiles.filter((currentPromise) => {
                    isError = (isError.length === 0 && currentPromise.status === 'rejected') ? currentPromise.reason : isError;
                    return currentPromise.status === 'fulfilled';
                })
                    .map((currentPromise) => {
                        return currentPromise.value;
                    });

                if (fullfilledFiles.length === fileList.length) {
                    status = 200;
                    successMessage.message = 'All files deleted successfully';

                } else if (fullfilledFiles.length === 0) {
                    reject(isError);
                } else {
                    successMessage["files deleted"] = fullfilledFiles;
                }
                resolve([status, successMessage]);
            });
    });
}

module.exports = deleteAllFiles;