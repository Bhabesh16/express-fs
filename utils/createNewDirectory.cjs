const fs = require('fs');
const path = require('path');

function createNewDirectory(directoryName) {

    return new Promise((resolve, reject) => {

        if (directoryName.length === 0) {
            reject([400, "directory name is empty"]);
        } else if (directoryName.includes("/")) {
            reject([400, "cannot include / in directory name"]);
        } else {
            const directoryPath = path.join(__dirname, "../public", directoryName);

            fs.mkdir(directoryPath, { recursive: true }, (err) => {
                if (err) {
                    console.error(err);
                    reject([400, err.message]);
                } else {
                    resolve();
                }
            });
        }
    });
}

module.exports = createNewDirectory;