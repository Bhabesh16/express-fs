const fs = require('fs');
const path = require('path');

function createNewFiles(directoryName, fileList) {

    return new Promise((resolve, reject) => {

        const promiseCreateFiles = fileList.map((fileName) => {

            return new Promise((resolve, reject) => {

                if (typeof fileName !== 'string' || fileName.trim().length < 1 || fileName.includes('/') || fileName.includes('.')) {
                    console.error(new Error("File name is incorrect"));
                    reject([400, 'file name is incorrect in fileList']);
                } else {

                    const data = {
                        name: fileName
                    };

                    const filePath = path.join(__dirname, '../public', directoryName, `${fileName}.json`);

                    fs.writeFile(filePath, JSON.stringify(data), (err) => {
                        if (err) {
                            console.error(err);
                            reject([400, err.message]);
                        } else {
                            resolve(fileName);
                        }
                    });
                }
            });
        });

        Promise.allSettled(promiseCreateFiles)
            .then((promiseFiles) => {
                const successMessage = {
                    message: 'some files created'
                };

                let status = 206;
                let isError = [];

                const allFullfilledFiles = promiseFiles.filter((currentPromise) => {
                    isError = (isError.length === 0 && currentPromise.status === 'rejected') ? currentPromise.reason : isError;
                    return currentPromise.status === 'fulfilled';
                })
                    .map((currentPromise) => {
                        return currentPromise.value;
                    });

                if (allFullfilledFiles.length === fileList.length) {
                    status = 200;
                    successMessage.message = 'All files created successfully';

                } else if (allFullfilledFiles.length === 0) {
                    reject(isError);
                } else {
                    successMessage["files created"] = allFullfilledFiles;
                }
                resolve([status, successMessage]);
            });
    });
}

module.exports = createNewFiles;